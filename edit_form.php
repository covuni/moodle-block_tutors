<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Tutors edit_form file for local instance configuration.
 *
 * @package    block_tutors
 * @author     Manoj Solanki
 * @copyright  2020 Manoj Solanki (Coventry University)
 *
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Tutors block instance settings form.
 *
 * @package   block_tutors
 * @copyright 2020 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_tutors_edit_form extends block_edit_form {

    /**
     * Define the form.
     *
     * @param object $mform
     */
    protected function specific_definition($mform) {

        // Block height.
        $mform->addElement('text', 'config_title', get_string('title', 'block_tutors'));
        $mform->setDefault('config_title', '');
        $mform->setType('config_title', PARAM_TEXT);
        $mform->addHelpButton('config_title', 'title', 'block_tutors');

        $mform->addElement('text', 'config_blockheight', get_string('blockheight', 'block_tutors'));
        $mform->setDefault('config_blockheight', BLOCK_TUTORS_SETTINGS_DEFAULT_HEIGHT);
        $mform->setType('config_blockheight', PARAM_INTEGER);
        $mform->addHelpButton('config_blockheight', 'blockheight', 'block_tutors');

        $mform->addElement('text', 'config_minwidthcard', get_string('minwidthcard', 'block_tutors'));
        $mform->setDefault('config_minwidthcard', BLOCK_TUTORS_SETTINGS_MIN_WIDTH_CARD);
        $mform->setType('config_minwidthcard', PARAM_INTEGER);
        $mform->addHelpButton('config_minwidthcard', 'minwidthcard', 'block_tutors');

        $mform->addElement('text', 'config_maxcolsperrow', get_string('maxcolsperrow', 'block_tutors'));
        $mform->setDefault('config_maxcolsperrow', BLOCK_TUTORS_SETTINGS_DEFAULT_MAX_COLS_PER_ROW);
        $mform->setType('config_maxcolsperrow', PARAM_INTEGER);
        $mform->addHelpButton('config_maxcolsperrow', 'maxcolsperrow', 'block_tutors');

    }
}