<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains language strings used for the Tutors block
 *
 * @package block_tutors
 * @copyright 2019 Manoj Solanki
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Tutors';
$string['blocktitle'] = 'Tutors';

$string['tutors:addinstance'] = 'Add a Tutors block';
$string['tutors:myaddinstance'] = 'Add a Tutors block to the My Moodle page.';

// Instance related settings.
$string['title'] = 'Block title';
$string['title_help'] = 'Title for Tutors block.  Leave empty to not show one (default).';

$string['blockheight'] = 'Height of block. Enter a number (uses px unit)';
$string['blockheight_help'] = '. Enter 0 for no height restriction. This shows a scrollbar where appropriate';
$string['minwidthcard'] = 'Minimum width of a each tutor profile space (uses px unit)';
$string['minwidthcard_help'] = 'This will determine the minimum width each tutor profile will take, in pixels. Choose a higher number for larger widths.';
$string['maxcolsperrow'] = 'Max number of Tutors per row';
$string['maxcolsperrow_help'] = 'Choose the number of tutors to show in one row, on medium to large screen devices. It will show less columns for smaller devices.';

$string['userdetailstoshowheading'] = 'User details to show';
$string['userdetailstoshowtext'] = 'Select the information about tutors to show here.';
$string['showemail'] = 'Show email address';
$string['showemaildesc'] = 'Show the email address';
$string['showmessagetutor'] = 'Show Message link';
$string['showmessagetutordesc'] = 'Show a link to message the tutor';
$string['showphone'] = 'Show phone number';
$string['showphonedesc'] = 'Show phone number of tutor';
$string['showdescription'] = 'Show description';
$string['showdescriptiondesc'] = 'Show description (bio) of tutor';
$string['maxlengthdescription'] = 'Max length of description';
$string['maxlengthdescriptiondesc'] = 'Set the maximum displayed length before a “read more” toggle is displayed. This is useful to keep display of individual tutors consistent regardless of the amount of text they enter into this field.';
$string['showalternatename'] = 'Show alternate name';
$string['showalternatenamedesc'] = 'Show alternate name instead, where available';
$string['showinheritedusers'] = 'Show inherited users';
$string['showinheritedusersdesc'] = 'Show users with inherited roles';
$string['block_tutors_custom_profile_fields_mapping_heading'] = 'Custom Profile Field Mapping';
$string['block_tutors_custom_profile_fields_mapping_text'] = 'Set the below options to map certain other useful fields to custom profile fields, where available';
$string['profilefieldfortitle'] = 'Profile field for showing Title / Job Role';
$string['profilefieldfortitledesc'] = 'Choose a custom profile field for showing a user\'s title / job role. For example "Lecturer in Philosophy".';
$string['profilefieldforlocation'] = 'Profile field for showing Location / Office';
$string['profilefieldforlocationdesc'] = 'Choose a custom profile field for showing a user\'s location / office.';
$string['profilefieldforofficehours'] = 'Profile field for showing Office hours';
$string['profilefieldforofficehoursdesc'] = 'Choose a custom profile field for showing a user\'s office hours';
$string['maxlengthofficehours'] = 'Max length of office hours';
$string['maxlengthofficehoursdesc'] = 'Set the maximum displayed length before a “read more” toggle is displayed. This is useful to keep display of individual tutors consistent regardless of the amount of text they enter into this field.';
$string['alloweditprofile'] = 'Allow tutors to edit their own profile';
$string['alloweditprofiledesc'] = 'Allow tutors who see the block to edit their own profile';
$string['itsyou'] = 'It\'s you!';
$string['showtaughtcourses'] = 'Show enrolled courses';
$string['showtaughtcoursesdesc'] = 'Optionally, show all the courses this contact is enrolled on.';

$string['showtheserolesheading'] = 'Available Roles';
$string['showtheserolestext'] = 'Choose the available roles to show';
$string['showtheseroles'] = 'Show these roles';
$string['showtheserolesdesc'] = 'Select the roles to show. To select more than one or deselect a role, use the \'Ctrl\' key';

$string['sorttutorsby'] = 'Order for display of the tutors';
$string['sorttutorsbydesc'] = 'Choose the order in which to display the tutors. E.g. by surname, time of last update of profile, etc.';

$string['bsstylesheading'] = 'Styling options (based on Bootstrap 4)';
$string['bsstylestext'] = 'The options below allow for extra styling settings based on Boostrap 4.0 CSS classes. See the screenshot below for further information about the color names. Choose default to use the block\'s own defined colors. Also see https://getbootstrap.com/docs/4.0/utilities/colors/ for further information.';
$string['bsbackgroundcolor'] = 'Background color';
$string['bsbackgroundcolordesc'] = 'Background color for tutor container.';
$string['bstextcolor'] = 'Text color';
$string['bstextcolordesc'] = 'Text color for each tutor container.';
$string['bstextalign'] = 'Text Alignment';
$string['bstextaligndesc'] = 'Text alignment for each tutor container.';

$string['editprofilebuttontext'] = 'Edit My Profile';
$string['viewprofilebuttontext'] = 'View My Profile';

$string['aria:controls'] = 'Tutors Block controls';
$string['aria:tutorsfilterdropdown'] = 'Tutors filter drop-down menu';
$string['aria:tutorseditprofilebutton'] = 'Edit my profile';


// Other text displayed.
$string['readmoretext'] = 'Read more';
$string['showlesstext'] = 'Show less';
$string['descriptiontitle'] = 'Description';
$string['officehourstitle'] = 'Office hours';
$string['locationtitle'] = 'Office';
$string['jobroletitle'] = 'Job title / role';
$string['editmyprofileformtitle'] = 'Edit My Profile';
$string['cannotdisplayeditprofileform'] = 'Unable to display Edit Profile form';
$string['cannoteditownprofile'] = 'Sorry, you do not have permission to edit your own profile.';
$string['sendemailtext'] = 'Send an email using an email client (if available)';
$string['saveuserprofileinfo'] = 'NOTE: When saving the profile information, it may take a while to reflect correctly in the list of contacts due to caching';
$string['saveddatamessage'] = '<h1>Saved Profile! Reloading page</h1><br>If the redirect does not happen within a few seconds, please {$a}';
$string['datacouldnotbesavedmessage'] = 'Profile could not be saved. Please check';
$string['enrolledcourses'] = 'Enrolled courses / modules';
$string['nocontentdisplaymessagetext'] = 'Any Tutors for your modules / courses, will appear here. ';
$string['allcoursesstr'] = 'All Courses';
$string['telephonenumbertitle'] = 'Tel: ';

/*
 * Cache settings.
 */

$string['cachedetailsheading'] = 'Caching';
$string['cachedetailstext'] = 'Cache options.';
$string['usecaching'] = 'Use caching';
$string['usecachingdesc'] = 'Switch on caching (uses Moodle cache API). Strongly recommended! Particularly if you have a larger site.';
$string['cachingttl'] = 'Cache Expiry time';
$string['cachingttldesc'] = 'Cache expiry time in seconds (TTL)';