<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * Settings for Tutors block.
 *
 * @package   block_tutors
 * @copyright 2019 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once("$CFG->dirroot/user/profile/lib.php");

if ($ADMIN->fulltree) {
    global $OUTPUT, $PAGE;

    $bscolorchoices = array (
        BLOCK_TUTORS_SETTING_NONE => 'None',
        BLOCK_TUTORS_BS_COLOR_DARK => 'Dark',
        BLOCK_TUTORS_BS_COLOR_LIGHT => 'Light',
        BLOCK_TUTORS_BS_COLOR_PRIMARY => 'Primary',
        BLOCK_TUTORS_BS_COLOR_SECONDARY => 'Secondary',
        BLOCK_TUTORS_BS_COLOR_SUCCESS => 'Success',
        BLOCK_TUTORS_BS_COLOR_DANGER => 'Danger',
        BLOCK_TUTORS_BS_COLOR_WARNING => 'Warning',
        BLOCK_TUTORS_BS_COLOR_INFO => 'Info'
    );

    $bsalignchoices = array (
        BLOCK_TUTORS_BS_ALIGN_LEFT => 'Left',
        BLOCK_TUTORS_BS_ALIGN_CENTER => 'Center',
        BLOCK_TUTORS_BS_ALIGN_RIGHT => 'Right'
    );

    $sorttutorschoices = array (
        BLOCK_TUTORS_SORT_SURNAME => 'Surname',
        BLOCK_TUTORS_SORT_LAST_ACCESSED => 'Last Access',
        BLOCK_TUTORS_SORT_LAST_UPDATED => 'Last updated'
    );

    // Sort options.
    $settings->add(new admin_setting_configselect('block_tutors/sorttutorsby',
        get_string('sorttutorsby', 'block_tutors'),
        get_string('sorttutorsbydesc', 'block_tutors'),
        BLOCK_TUTORS_SORT_SURNAME, $sorttutorschoices));

    // User details to show.
    $settings->add(new admin_setting_heading('block_tutors_userdetailstoshow', get_string('userdetailstoshowheading',
                   'block_tutors'), get_string('userdetailstoshowtext', 'block_tutors')));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showemail',
        get_string('showemail', 'block_tutors'),
        get_string('showemaildesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showmessagetutor',
        get_string('showmessagetutor', 'block_tutors'),
        get_string('showmessagetutordesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showphone',
        get_string('showphone', 'block_tutors'),
        get_string('showphonedesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showdescription',
        get_string('showdescription', 'block_tutors'),
        get_string('showdescriptiondesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configtext('block_tutors' . '/maxlengthdescription',
            get_string('maxlengthdescription', 'block_tutors'),
            get_string('maxlengthdescriptiondesc', 'block_tutors'), BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH, PARAM_INT));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showalternatename',
        get_string('showalternatename', 'block_tutors'),
        get_string('showalternatenamedesc', 'block_tutors'), 0));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/alloweditprofile',
            get_string('alloweditprofile', 'block_tutors'),
            get_string('alloweditprofiledesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/showtaughtcourses',
            get_string('showtaughtcourses', 'block_tutors'),
            get_string('showtaughtcoursesdesc', 'block_tutors'), 0));

    // Custom profile field mapping for other information.
    $allcustomprofilefields = profile_get_custom_fields();
    $profilefieldchoices = array(BLOCK_TUTORS_SETTING_NONE => 'None');

    foreach ($allcustomprofilefields as $customprofilefield) {
        $fieldname = mb_strimwidth($customprofilefield->name, 0, BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH, '...', 'utf-8');
        $profilefieldchoices[$customprofilefield->shortname] = $fieldname . ' (' . $customprofilefield->shortname . ')';
    }

    $settings->add(new admin_setting_heading('block_tutors_custom_profile_fields_mapping',
            get_string('block_tutors_custom_profile_fields_mapping_heading', 'block_tutors'),
            get_string('block_tutors_custom_profile_fields_mapping_text', 'block_tutors')));

    $settings->add(new admin_setting_configselect('block_tutors/profilefieldfortitle',
            get_string('profilefieldfortitle', 'block_tutors'),
            get_string('profilefieldfortitledesc', 'block_tutors'),
            BLOCK_TUTORS_SETTING_NONE, $profilefieldchoices));

    $settings->add(new admin_setting_configselect('block_tutors/profilefieldforlocation',
            get_string('profilefieldforlocation', 'block_tutors'),
            get_string('profilefieldforlocationdesc', 'block_tutors'),
            BLOCK_TUTORS_SETTING_NONE, $profilefieldchoices));

    $settings->add(new admin_setting_configselect('block_tutors/profilefieldforofficehours',
            get_string('profilefieldforofficehours', 'block_tutors'),
            get_string('profilefieldforofficehoursdesc', 'block_tutors'),
            BLOCK_TUTORS_SETTING_NONE, $profilefieldchoices));

    $settings->add(new admin_setting_configtext('block_tutors' . '/maxlengthofficehours',
            get_string('maxlengthofficehours', 'block_tutors'),
            get_string('maxlengthofficehoursdesc', 'block_tutors'), BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH, PARAM_INT));

    $settings->add(new admin_setting_heading('block_tutors_showtheseroles', get_string('showtheserolesheading', 'block_tutors'),
            get_string('showtheserolestext', 'block_tutors')));

    // Third section builds a list of the roles available within this context for selection.
    $roles = get_all_roles();
    $roles = role_get_names();

    // Get all roles.
    $rolestoshow = array();
    $default = array();

    foreach ($roles as $role) {
        $rolestoshow[$role->id] = $role->localname;
    }

    $settings->add(new admin_setting_configmultiselect('block_tutors/rolestoshow', get_string('showtheseroles', 'block_tutors'),
                                                get_string('showtheserolesdesc', 'block_tutors'),  array(), $rolestoshow));

    // Caching.
    $settings->add(new admin_setting_heading('block_tutors_cache', get_string('cachedetailsheading', 'block_tutors'),
            get_string('cachedetailstext', 'block_tutors')));

    $settings->add(new admin_setting_configcheckbox('block_tutors' . '/usecaching',
        get_string('usecaching', 'block_tutors'),
        get_string('usecachingdesc', 'block_tutors'), 1));

    $settings->add(new admin_setting_configtext('block_tutors' . '/cachingttl',
        get_string('cachingttl', 'block_tutors'),
        get_string('cachingttldesc', 'block_tutors'), TUTORS_CACHE_TTL_DEFAULT, PARAM_INT));

    $bscolorsimageurl = '<img src="' . $OUTPUT->image_url('bscolors', 'block_tutors')->out() . '">';
    $settings->add(new admin_setting_heading('block_tutors_bsheading', get_string('bsstylesheading', 'block_tutors'),
                                             get_string('bsstylestext', 'block_tutors') . '<br><br>' . $bscolorsimageurl));

    $settings->add(new admin_setting_configselect('block_tutors/bstextcolor',
        get_string('bstextcolor', 'block_tutors'),
        get_string('bstextcolordesc', 'block_tutors'),
        BLOCK_TUTORS_SETTING_NONE, $bscolorchoices));

    $settings->add(new admin_setting_configselect('block_tutors/bsbackgroundcolor',
        get_string('bsbackgroundcolor', 'block_tutors'),
        get_string('bsbackgroundcolordesc', 'block_tutors'),
        BLOCK_TUTORS_SETTING_NONE, $bscolorchoices));

    $settings->add(new admin_setting_configselect('block_tutors/bstextalign',
        get_string('bstextalign', 'block_tutors'),
        get_string('bstextaligndesc', 'block_tutors'),
        BLOCK_TUTORS_BS_ALIGN_LEFT, $bsalignchoices));

}