<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines the web service calls utilised by the block to perform the required functions.
 *
 * @package    block_tutors
 * @copyright  2019 Coventry University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Manoj Solanki (Coventry University)
 */

defined('MOODLE_INTERNAL') || die();

$functions = array(
    'external_block_tutors_get_tutors_for_courses' => array(
        'classname'   => 'block_tutors_external',
        'methodname'  => 'external_block_tutors_get_tutors_for_courses',
        'classpath'   => 'blocks/tutors/externallib.php',
        'description' => 'Get tutors for courses.',
        'type'        => 'read',
        'ajax'          => true,
        'loginrequired' => true
    )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
    'Tutors' => array(
        'functions' => array ('external_block_tutors_get_tutors_for_courses',
        'restrictedusers' => 0,
        'enabled' => 1
        )
    )
);
