// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Javascript functions.
 *
 * @module     block_tutors/tutors
 * @class      tutors
 * @package    block_tutors
 * @copyright  2020 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      3.6
 */

define(['jquery', 'core/ajax', 'core/custom_interaction_events', 'core/fragment', 'core/notification'],
function($, ajax, CustomEvents, Fragment, Notification) {

    return {

        init: function(uniqid, contextid) {

            $(document).ready(function() {
                window.onbeforeunload = null;
            });

            var tutorblockopenid = 0;

            $(document).on( 'keydown', function(e) {
                if (e.keyCode === 27) { // Escape key.
                    if (tutorblockopenid != 0) {
                        $('#tutor-edit-' + tutorblockopenid).removeClass('d-flex');
                        $('#tutor-edit-' + tutorblockopenid).hide();
                    }
                }
            });

            // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/includes#Polyfill
            // This has been added as .includes function is only available in ES6, meaning no IE or old browser support.
            if (!String.prototype.includes) {
                String.prototype.includes = function(search, start) {
                    'use strict';

                    if (search instanceof RegExp) {
                        throw TypeError('first argument must not be a RegExp');
                    }

                    if (start === undefined) {
                        start = 0;
                    }
                    return this.indexOf(search, start) !== -1;
                };
            }

            // Handle all click events.
            $(".block-tutors").on("click", function(event) {

                if (event.target.getAttribute("class") == 'block_tutors_read_more') {
                    event.preventDefault();
                    var readmoreid = $(event.target).attr('data-id');
                    $('#text-full-' + readmoreid).show();
                    $('#show-less-link-' + readmoreid).show();
                    $('#text-excerpt-' + readmoreid).hide();
                    $('#read-more-link-' + readmoreid).hide();

                } else if (event.target.getAttribute("class") == 'block_tutors_show_less') {
                    event.preventDefault();
                    var showlessid = $(event.target).attr('data-id');
                    $('#text-full-' + showlessid).hide();
                    $('#show-less-link-' + showlessid).hide();
                    $('#text-excerpt-' + showlessid).show();
                    $('#read-more-link-' + showlessid).show();

                } else if ($(event.target).hasClass("tutor-body-edit-button-edit")) {

                    event.preventDefault();
                    var userid = $(event.target).attr('data-id');
                    $('#tutor-edit-' + userid).addClass('d-flex');
                    $('#tutor-edit-' + userid).show();
                    tutorblockopenid = userid;

                } else if ($(event.target).hasClass("block_tutors_overlay_close_icon")) {
                    event.preventDefault();
                    var id = $(event.target).attr('data-id');
                    $('#tutor-edit-' + id).removeClass('d-flex');
                    $('#tutor-edit-' + id).hide(500);

                } else if (event.target.getAttribute("type") == 'submit') {

                    event.preventDefault();
                    var form = $(event.target).closest(".tutor-edit-overlay-form form");
                    var data = form.serialize();
                    var dataArray = form.serializeArray();
                    var dataObj = {};

                    $(dataArray).each(function(i, field) {
                        dataObj[field.name] = field.value;
                    });

                    // Call the save profile method that is defined in lib.php.
                    Fragment.loadFragment(
                        'block_tutors',
                        'save_profile',
                        contextid,
                        {
                            querystring: data,
                            userid: dataObj.userid
                        }
                    ).done(function(html) {
                        $("#tutor-edit-overlay-" + dataObj.userid).html(html);
                        if (!html.includes('Error')) {

                            window.location = window.location.href.split("#")[0];
                        }
                    })
                    .fail(Notification.exception);

                }

            });

            /**
             * Event listener for course filter.
             *
             */
            var SELECTORS = {
                FILTERS: '[data-region="tutors-filter"]',
                FILTER_OPTION: '[data-tutors-filter]'
            };

            var Selector = $('#' + uniqid).find(SELECTORS.FILTERS);

            CustomEvents.define(Selector, [CustomEvents.events.activate]);
            Selector.on(
                CustomEvents.events.activate,
                SELECTORS.FILTER_OPTION,
                function(e, data) {
                    var option = $(e.target);

                    if (option.hasClass('active')) {
                        // If it's already active then we don't need to do anything.
                        return;
                    }

                    var chosenfiltervalue = option.attr('data-value');
                    var minwidthcard = option.attr('data-min-width-card');
                    var maxcolsperrow = option.attr('data-max-cols-per-row');

                    ajax.call([{
                        methodname: 'external_block_tutors_get_tutors_for_courses',
                        args: {'courseid': chosenfiltervalue,
                               'minwidthcard' : minwidthcard,
                               'maxcolsperrow' : maxcolsperrow},
                        done:
                            function() {
                                var result = arguments[0];
                                $("#tutors-content-" + uniqid).html(result.html);
                            },

                        fail: function(ex) {
                            Notification.exception(ex);
                            $("#tutors-content-" + uniqid).html(ex);
                        }
                    }]);

                    data.originalEvent.preventDefault();
                }
            );

        }

    };
});
