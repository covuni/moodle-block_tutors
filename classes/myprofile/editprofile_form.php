<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit profile class.
 *
 * @package    block_tutors
 * @copyright  2020 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

namespace block_tutors\myprofile;

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->dirroot.'/user/editlib.php');

/**
 * Class editprofile_form.
 *
 * @package   block_tutors
 * @copyright Copyright (c) 2020 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class editprofile_form extends \moodleform {

    /**
     * Define the form.
     */
    public function definition() {
        global $USER, $CFG, $COURSE, $OUTPUT;

        $mform = $this->_form;
        $editoroptions = null;
        $filemanageroptions = null;

        $config = get_config('block_tutors');

        if (!is_array($this->_customdata)) {
            throw new coding_exception('invalid custom data for user_edit_form');
        }

        $editoroptions = $this->_customdata['editoroptions'];
        $filemanageroptions = $this->_customdata['filemanageroptions'];
        $user = $this->_customdata['user'];
        $userid = $user->id;

        // Add some extra hidden fields.
        $mform->addElement('hidden', 'id');
        $mform->setType('id', \core_user::get_property_type('id'));
        $mform->addElement('hidden', 'course', $COURSE->id);
        $mform->setType('course', PARAM_INT);

        // Now add user form data.
        if ($user->id > 0) {
            useredit_load_preferences($user, false);
        }

        if (empty($USER->newadminuser)) {
            $mform->addElement('static', 'moodle_picture', '<h3>'.get_string('pictureofuser').'</h3>');

            if (!empty($CFG->enablegravatar)) {
                $mform->addElement('html', \html_writer::tag('p', get_string('gravatarenabled')));
            }

            $mform->addElement('static', 'currentpicture', get_string('currentpicture'));

            $mform->addElement('checkbox', 'deletepicture', get_string('deletepicture'));
            $mform->setDefault('deletepicture', 0);

            $mform->addElement('filemanager', 'imagefile', get_string('newpicture'),
                    'class="adaptablemyeditprofile"', $filemanageroptions);
            $mform->addHelpButton('imagefile', 'newpicture');

            $mform->addElement('text', 'imagealt', get_string('imagealt'),
                    'maxlength="100" size="54" class="adaptablemyeditprofile"');
            $mform->setType('imagealt', PARAM_TEXT);
        }

        if ($user->id > 0) {
            useredit_load_preferences($user, false);
        }

        $stringman = get_string_manager();

        $mform->addElement('editor', 'description_editor', get_string('userdescription'),
                'class="adaptablemyeditprofile"', $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);
        $mform->addHelpButton('description_editor', 'userdescription');

        // Profile fields. Check and display any profile fields.
        $profilefieldsallowed = array();
        //
        // Optional profile fields, if they are filled in.
        //
        $userprofilefields = profile_user_record($user->id, false);

        // Job role / title.
        if ( (!empty($config->profilefieldfortitle)) &&
            ($config->profilefieldfortitle != 'none') ) {

            $profilefieldfortitle = $config->profilefieldfortitle;
            $profilefieldsallowed[] = $profilefieldfortitle;

        }

        // Location / Office.
        if ( (!empty($config->profilefieldforlocation)) &&
            ($config->profilefieldforlocation != 'none') ) {

            $profilefieldforlocation = $config->profilefieldforlocation;
            $profilefieldsallowed[] = $profilefieldforlocation;

        }

        // Office hours.
        if ( (!empty($config->profilefieldforofficehours)) &&
            ($config->profilefieldforofficehours != 'none') ) {

            $profilefieldofficehours = $config->profilefieldforofficehours;
            $profilefieldsallowed[] = $profilefieldofficehours;

        }

        $fields = profile_get_user_fields_with_data($userid);

        foreach ($fields as $formfield) {
            if ($formfield->is_editable()) {
                if (in_array($formfield->field->shortname, $profilefieldsallowed)) {
                    $formfield->edit_field($mform);
                }
            }
        }

        // Add some extra hidden fields.
        $mform->addElement('hidden', 'userid', $userid);
        $mform->setType('userid', PARAM_INT);
        $mform->addElement('hidden', 'course', $COURSE->id);
        $mform->setType('course', PARAM_INT);

        $btnstring = get_string('updatemyprofile');

        $mform->addElement('static', 'info', get_string('saveuserprofileinfo', 'block_tutors'));
        $this->add_action_buttons(false, $btnstring);
        $this->set_data($user);
    }

    /**
     * Extend the form definition after data has been parsed.
     */
    public function definition_after_data() {
        global $USER, $CFG, $DB, $OUTPUT;

        $mform = $this->_form;

        // Trim required name fields.
        foreach (useredit_get_required_name_fields() as $field) {
            $mform->applyFilter($field, 'trim');
        }

        if ($userid = $mform->getElementValue('id')) {
            $user = $DB->get_record('user', array('id' => $userid));
        } else {
            $user = false;
        }

        // Print picture.
        if (empty($USER->newadminuser)) {
            if ($user) {
                $context = \context_user::instance($user->id, MUST_EXIST);
                $fs = get_file_storage();
                $hasuploadedpicture = ($fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.png')
                                      || $fs->file_exists($context->id, 'user', 'icon', 0, '/', 'f2.jpg'));

                if (!empty($user->picture) && $hasuploadedpicture) {
                    $imagevalue = $OUTPUT->user_picture($user, array('courseid' => SITEID, 'size' => 64));
                } else {
                    $imagevalue = get_string('none');
                }
            } else {
                $imagevalue = get_string('none');
            }

            $imageelement = $mform->getElement('currentpicture');
            $imageelement->setValue($imagevalue);

            if ($user && $mform->elementExists('deletepicture') && !$hasuploadedpicture) {
                $mform->removeElement('deletepicture');
            }
        }

        // Next the customisable profile fields.
        profile_definition_after_data($mform, $userid);
    }

    /**
     * Validate the form data.
     * @param array $usernew
     * @param array $files
     * @return array|bool
     */
    public function validation($usernew, $files) {
        global $CFG, $DB;

        return array();

        $usernew = (object)$usernew;

        $user = $DB->get_record('user', array('id' => $usernew->id));
        $err = array();

        // Next the customisable profile fields.
        $err += profile_validation($usernew, $files);

        if (count($err) == 0) {
            return true;
        } else {
            return $err;
        }
    }

}
