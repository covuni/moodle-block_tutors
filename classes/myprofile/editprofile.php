<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Edit profile class. Utilise Moodle class autoloading by putting any edit profile form methods here where possible.
 *
 * @package    block_tutors
 * @copyright  2020 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
namespace block_tutors\myprofile;

use context_system;
use context_user;
use core_tag_tag;
use moodle_url;

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

/**
 * Class editprofile. This holds any data and methods needed for using the edit profile form.
 *
 * @package   block_tutors
 * @copyright Copyright (c) 2020 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class editprofile {

    /**
     * This is to generate the user profile edit form.
     *
     * @param int    $userid User id
     * @param mixed  $ajaxformdata null or an array of params if the form is being generated from an ajax call
     * @return string The rendered mform fragment.
     */
    public static function generate_tutor_edit_form($userid, $ajaxformdata = null) {
        global $CFG, $DB, $PAGE, $SITE, $USER;

        $systemcontext = context_system::instance();

        // Editing existing user.
        $user = $DB->get_record('user', array('id' => $userid), '*', MUST_EXIST);

        $usercontext = context_user::instance($user->id);

        // Check that the user can edit their own profile.
        if (!has_capability('moodle/user:editownprofile', $usercontext)) {
            return null;
        }

        // Load user preferences.
        useredit_load_preferences($user);

        // Load custom profile fields data.
        profile_load_data($user);

        // User interests.
        $user->interests = core_tag_tag::get_item_tags_array('core', 'user', $userid);

        // This will never happen really as $user is expected to exist (due to the above code getting user record).
        if ($user->id !== -1) {

            $editoroptions = array(
                    'maxfiles'   => EDITOR_UNLIMITED_FILES,
                    'maxbytes'   => $CFG->maxbytes,
                    'trusttext'  => false,
                    'forcehttps' => false,
                    'context'    => $usercontext
            );

            $user = file_prepare_standard_editor($user, 'description', $editoroptions, $usercontext, 'user', 'profile', 0);
        } else {
            $usercontext = null;
            // This is a new user, we don't want to add files here.
            $editoroptions = array(
                    'maxfiles' => 0,
                    'maxbytes' => 0,
                    'trusttext' => false,
                    'forcehttps' => false,
                    'context' => $coursecontext
            );
        }

        // Prepare filemanager draft area.
        $draftitemid = 0;
        $filemanagercontext = $editoroptions['context'];
        $filemanageroptions = array('maxbytes'       => $CFG->maxbytes,
                'subdirs'        => 0,
                'maxfiles'       => 1,
                'accepted_types' => 'web_image');
        file_prepare_draft_area($draftitemid, $filemanagercontext->id, 'user', 'newicon', 0, $filemanageroptions);
        $user->imagefile = $draftitemid;

        // Create form.
        $method = 'post';
        $target = '';
        $attributes = null;
        $editable = true;

        $mform = new editprofile_form(new moodle_url($PAGE->url), array(
                'editoroptions' => $editoroptions,
                'filemanageroptions' => $filemanageroptions,
                'user' => $user),
                $method,
                $target,
                $attributes,
                $editable,
                $ajaxformdata
                );

        return array(
                'form' => $mform,
                'user' => $user,
                'editoroptions' => $editoroptions,
                'filemanageroptions' => $filemanageroptions,
                'usercontext' => $usercontext
        );
    }
}