# Moodle Tutors block #

This block displays all the tutors for user's courses. It works on the selecting roles to display for users that are defined as 'tutors' within the block settings.

## Guidelines for use ##

This block works on a dashboard, frontpage or course page. When on a dashboard, it will display tutors across all the user's enrolled courses. On a course page, it will display tutors only for that course. 

This plugin has been designed to work with all themes, but works well with the Adaptable theme, as it contains block regions that can appear on a course page.

## Version ##

Version 1.2 (2020012200)

## Instance Settings ##

Instance-specific settings are as follows. This refers to settings applying to each individual block that is added.

### Block height ###

Configure the height of the block, in px. This allows a custom height for the block, or for unlimited height by entering 0.

## Site-wide settings ##
Site-wide configuration options are available under:  Site Administration > Plugins > Blocks > Tutors

For a more in-depth explanation of these, please see the user guide accompanying this plugin.

### Order for display of the tutors ###
Choose the order in which to display the tutors. E.g. by surname, time of last update of profile and last access.

### User details to show ###
Select the information about tutors to show here.

### Show email address ###
Toggle for showing an icon that displays the email address in a tool tip on hover. The email address can be clicked and will open up a default email client where available.

### Show Message link ###
Toggle for showing a message icon, allowing users to message that tutor.

### Show phone number ###
Toggle for showing the phone number

### Show description ###
Toggle for showing the profile description 

### Max length of description ###
Set the maximum displayed length before a “read more” toggle is displayed. This is useful to keep display of individual tutors consistent regardless of the amount of text they enter into this field. Default is 50 characters.

### Show alternate name ###
Show alternate name instead, where available

### Allow tutors to edit their own profile ###
Allow tutors who see the block to edit their own profile

### Show enrolled courses ###
Optionally, show all the courses this contact is enrolled on. Applies only to index pages - Dashboard and frontpage

### Custom Profile Field Mapping Options ###
Set the below options to map certain other useful fields to custom profile fields, where available

### Profile field for showing Title / Job Role ###
Choose a custom profile field for showing a user's title / job role. For example "Lecturer in Philosophy".

### Profile field for showing Location / Office ###
Choose a custom profile field for showing a user's location / office.

### Profile field for showing Office hours ###
Choose a custom profile field for showing a user's office hours

### Max length of office hours ###
Set the maximum displayed length before a “read more” toggle is displayed. This is useful to keep display of individual tutors consistent regardless of the amount of text they enter into this field. Default is 30 characters

### Show these roles ###
Select one or more roles to show for courses. E.g. Teacher, Module leader, etc. This setting allows the selection of multiple Moodle and custom roles.

### Use caching ###
Toggle for caching (uses Moodle cache API). Strongly recommended! Particularly if you have a larger site.

### Cache Expiry time ###
Cache expiry time in seconds (TTL). Default is 300 seconds.

### Styling options (based on Bootstrap 4) ###
The options below allow for extra styling settings based on Boostrap 4.0 CSS classes. Choose default to use the block's own defined colors. Also see https://getbootstrap.com/docs/4.0/utilities/colors/ for further information.

### Text color ###
Text color for each tutor container.

### Background color ###
Background color for tutor container.

### Text Alignment ###
Text alignment for each tutor container.

## Language strings ##

Below are the language strings available, along with their default values.

readmoretext  -  'Read more'

showlesstext  -  'Show less'

descriptiontitle  -  'Description'

officehourstitle  -  'Office hours'

locationtitle  -  'Office'

jobroletitle  -  'Job title / role'

editmyprofileformtitle  -  'Edit My Profile'

cannotdisplayeditprofileform  -  'Unable to display Edit Profile form'

cannoteditownprofile  -  'Sorry, you do not have permission to edit your own profile.'

sendemailtext  -  'Send an email using an email client (if available)'

saveuserprofileinfo  -  'NOTE: When saving the profile information, it may take a while to reflect correctly in the list of contacts due to caching'

saveddatamessage  -  '<h1>Saved Profile! Reloading page</h1><br>If the redirect does not happen within a few seconds, please {$a}' 

datacouldnotbesavedmessage  -  'Profile could not be saved. Please check'

enrolledcourses  -  'Enrolled courses / modules'

nocontentdisplaymessagetext  -  'Any Tutors for your modules / courses, will appear here. '

allcoursesstr  -  'All Courses'

## Compatibility ##

- Moodle 3.6, 3.7, 3.8

This block has been developed and tested with Adaptable 2.0 onwards, which includes a block region that will specifically appear on the relevant pages.  It may work with other themes, however it will need to appear in an appropriate block region that renders on a frontpage, dashboard page and course homepage.

## Contribution ##

Developed by:

 * Manoj Solanki (Coventry University)

Co-maintained by:

 * Jeremy Hopkins (Coventry University)

## Licenses ##

Licensed under: GPL v3 (GNU General Public License) - http://www.gnu.org/licenses
