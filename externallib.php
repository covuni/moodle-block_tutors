<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External API.
 *
 * This is used by the block_tutors plugin itself in javascript to perform various functions such as
 * displaying discussion posts.
 *
 * @package    block_tutors
 * @copyright  2020 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/externallib.php");
require_once($CFG->dirroot . "/blocks/tutors/lib.php");

use context_system;
use external_api;
use external_function_parameters;
use external_value;
use external_single_structure;
use external_multiple_structure;

/**
 * External API class.
 *
 * @package    block_tutors
 * @copyright  2020 Manoj Solanki (Coventry University)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
class block_tutors_external extends external_api {

    /**
     * Returns description of external_block_tutors_get_tutors_for_courses() parameters.
     *
     * @return \external_function_parameters
     */
    public static function external_block_tutors_get_tutors_for_courses_parameters() {
        return new external_function_parameters(
            array(
                'courseid' => new external_value(PARAM_INT, 'Course ID', VALUE_DEFAULT, ''),
                'minwidthcard' => new external_value(PARAM_INT, 'Minimum width of a card', VALUE_DEFAULT, ''),
                'maxcolsperrow' => new external_value(PARAM_INT, 'Max number of colums per row', VALUE_DEFAULT, ''),
            )
        );
    }

    /**
     * Returns description of external_block_tutors_get_tutors_for_courses() result value.
     *
     * @return \external_description
     */
    public static function external_block_tutors_get_tutors_for_courses_returns() {
        return new external_single_structure(
            array(
                'eventaction'                     => new external_value(PARAM_TEXT, 'Event action status'),
                'warning'                         => new external_value(PARAM_RAW, 'Any warning messages'),
                'html'                            => new external_value(PARAM_RAW, 'Returned HTML format new content'),
                'errormessage'                    => new external_value(PARAM_RAW, 'Any error messages')
            )
        );

    }

    /**
     *  Main external Web service call to get tutors.
     *
     * @param int    $courseid
     * @param string $organisation
     * @return mixed
     */
    public static function external_block_tutors_get_tutors_for_courses($courseid = 0, $minwidthcard = 0, $maxcolsperrow = 0, $organisation = '') {

        global $USER, $PAGE, $DB, $OUTPUT, $COURSE;

        require_once(dirname(dirname(__DIR__)).'/config.php');
        $config = get_config("block_tutors");

        // Clean parameters.
        $params = self::validate_parameters(self::external_block_tutors_get_tutors_for_courses_parameters(), array(
            'courseid' => $courseid, 'minwidthcard' => $minwidthcard, 'maxcolsperrow' => $maxcolsperrow));

        if ($courseid == 0) {
            // Get all courses.
            $courses = enrol_get_all_users_courses($USER->id, true, 'id, shortname', 'visible DESC, sortorder ASC');
            $course = $COURSE;
        } else {
            // Get the courses for the specified course id.
            $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
            $courses[$course->id] = $course;
        }

        $tutorssortorder = BLOCK_TUTORS_SORT_SURNAME_FIRSTNAME;
        if (!empty($config->sorttutorsby)) {
            $tutorssortorder = $config->sorttutorsby;
        }

        require_login($course, true);

        $tutors = block_tutors_get_tutors_for_all_courses($courses, $tutorssortorder);

        $tutorsjson = new stdClass();

        // Get page type.
        $currentpage = block_tutors_get_current_page();

        // Organise tutors into a grid.

        // Get maximum columns per row.
        $currentcolumncount = 0;

        $tutorrows = array();
        $tutortemplist = array();
        $currentrow = 0;

        // The allow edit profile status is stored in the list of tutors, so it accurately picks up if the current logged in user
        // can edit their profile. This is based on the setting for editing profile and also ensuring that the logged in user is
        // actually a tutor (e.g. in the list of roles to display in the settings).
        $alloweditprofile = false;

        foreach ($tutors as $tutor) {

            $tutorrows[$currentrow]['rows'][$currentcolumncount] = $tutor;

            $currentcolumncount++;

            if ($currentcolumncount == $maxcolsperrow) {
                $currentrow++;
                $currentcolumncount = 0;
            }

            if ($USER->id == $tutor['id']) {
                if ($tutor['alloweditprofile']) {
                    $alloweditprofile = true;
                }
            }
        }

        $tutorsjson = block_tutors_get_config_data($tutorrows, $courses, 0, $currentpage, $alloweditprofile, $maxcolsperrow);
        $tutorsjson['minwidthcard'] = $minwidthcard;
        $tutorsjson['maxcolsperrow'] = $maxcolsperrow;

        $content = $OUTPUT->render_from_template('block_tutors/tutors-content', $tutorsjson);

        return array(
            'eventaction'      => 'tutorsreturned',
            'html'             => $content,
            'warning'          => '',
            'errormessage'     => ''
        );

        return $returnvalue;

    }

}
