<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle Tutors block.
 *
 * Display all users with tutor-like roles in a boostrap grid.
 *
 * @package block_tutors
 * @copyright 2020 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->dirroot . '/user/profile/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->libdir . '/externallib.php');
require_once(dirname(__FILE__) . '/lib.php');

/**
 * Tutors block implementation class.
 *
 * @package   block_tutors
 * @copyright 2020 Manoj Solanki (Coventry University)
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_tutors extends block_base {

    /** @var array The plugin name of the block */
    const BLOCKPLUGINNAME = 'block_tutors';

    /** @var string $uniqid Holds a unique id to use in a div during render.
     *  This is more for futureproofing potential multiple instances of block on a page.
     */
    private $uniqid = 0;

    /**
     * Adds title to block instance.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_tutors');
        $this->uniqid = html_writer::random_id('block-tutors-');
        $this->config = new stdClass();
    }

    /**
     * Set up any configuration data.
     *
     * The is called immediatly after init().
     */
    public function specialization() {
        $config = get_config("block_tutors");

        // Use the title as defined in plugin settings, if one exists.
        if (!empty($this->config->title)) {
            $this->title = $this->config->title;
        }
    }

    /**
     * Gets Javascript.
     */
    public function get_required_javascript() {
        parent::get_required_javascript();

        $this->page->requires->js_call_amd('block_tutors/tutors', 'init',
                                array('id' => $this->uniqid, 'contextid' => $this->context->id));
    }

    /**
     * Which page types this block may appear on.  This currently is set to allow it to display
     * on the front page, dashboard page and the course home page.
     */
    public function applicable_formats() {
        return array('site-index' => true, 'my' => true, 'course-view-*' => true);
    }

    /**
     * Get block instance content.
     */
    public function get_content() {
        global $CFG, $COURSE, $USER, $OUTPUT, $PAGE, $ME, $DB;

        if ($this->content !== null) {
            return $this->content;
        }

        $config = get_config("block_tutors");

        if (!isloggedin()) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';

        $tutorssortorder = BLOCK_TUTORS_SORT_SURNAME;
        if (!empty($config->sorttutorsby)) {
            $tutorssortorder = $config->sorttutorsby;
        }

        // Check page type.  This can display only on dashboard, frontpage or a course homepage.
        $currentpage = block_tutors_get_current_page();

        // If empty, then means we're not on a page where this block should be displayed.
        if (empty($currentpage)) {
            return $this->content;
        }

        if ($currentpage == 'coursepage') {
            $courses = $allcourses['courses'] = array($COURSE);
            $iscoursepage = true;
            $tutors = block_tutors_get_tutors_for_all_courses($courses, $tutorssortorder, $iscoursepage);
        } else {

            $allcourses = array();

            // Check if caching is being used.  If so, get all user's course contacts from cache if possible. Or add it to cache.
            if (!empty ($config->usecaching)) {
                $cache = cache::make('block_tutors', BLOCK_TUTORS_CACHE_MY_COURSES_DATA);
                $allcourses = $cache->get($USER->id);

                if ($allcourses === false) {
                    $allcourses['courses'] = enrol_get_all_users_courses($USER->id, true, 'id, shortname',
                                             'visible DESC, sortorder ASC');

                    // Variable $allcourses should be an array all the time.
                    if ( (is_array($allcourses['courses'])) &&
                         (!empty ($allcourses['courses'])) ) {
                        $allcourses['lastcachebuildtime'] = time();

                        $cache->set($USER->id, $allcourses);

                    }

                } else {

                    // Check if ttl expired for this user's courses.
                    $timenow = time();
                    if (isset ($config->cachingttl) ) {
                        $usercachettl = $config->cachingttl;
                    } else {
                        $usercachettl = TUTORS_CACHE_TTL_DEFAULT;
                    }

                    // This should always be set.
                    if (isset ($allcourses['lastcachebuildtime'])) {
                        if ($timenow > ($allcourses['lastcachebuildtime'] + $usercachettl) ) {

                            // Clear this user from cache.
                            $cache->delete($USER->id);
                        }
                    }
                }

            } else {
                $allcourses['courses'] = enrol_get_all_users_courses($USER->id, true, 'id, shortname',
                                         'visible DESC, sortorder ASC');
            }

            // Get tutors.
            $tutors = block_tutors_get_tutors_for_all_courses($allcourses['courses'], $tutorssortorder);

        }

        if (empty($tutors)) {
            return $this->content;
        }

        // Organise tutors into a grid.

        // Get maximum columns per row.
        $currentcolumncount = 0;

        $tutorrows = array();
        $tutortemplist = array();
        $currentrow = 0;


        // Max number of columns in a row. This is an instance-specific setting. This helps
        // with ensuring columns also show well according to the viewport of the parent container.
        $maxcolsperrow = 0;
        if (!empty($this->config->maxcolsperrow)) {
            $maxcolsperrow = $this->config->maxcolsperrow;
        } else {
            $maxcolsperrow = BLOCK_TUTORS_SETTINGS_DEFAULT_MAX_COLS_PER_ROW;
        }

        // The allow edit profile status is stored in the list of tutors, so it accurately picks up if the current logged in user
        // can edit their profile. This is based on the setting for editing profile and also ensuring that the logged in user is
        // actually a tutor (e.g. in the list of roles to display in the settings).
        $alloweditprofile = false;

        foreach ($tutors as $tutor) {

            $tutorrows[$currentrow]['rows'][$currentcolumncount] = $tutor;

            $currentcolumncount++;

            if ($currentcolumncount == $maxcolsperrow) {
                $currentrow++;
                $currentcolumncount = 0;
            }

            if ($USER->id == $tutor['id']) {
                if ($tutor['alloweditprofile']) {
                    $alloweditprofile = true;
                }
            }
        }

        // Min width of each tutor profile. This is an instance-specific setting. This helps
        // with ensuring columns show well according to the viewport of the parent container.
        $minwidthcard = 0;
        if (!empty($this->config->minwidthcard)) {
            $minwidthcard = $this->config->minwidthcard;
        } else {
            $minwidthcard = BLOCK_TUTORS_SETTINGS_MIN_WIDTH_CARD;
        }

        // Block height. This is an instance-specific setting.
        $blockheight = 0;
        if (!empty($this->config->blockheight)) {
            $blockheight = $this->config->blockheight;
        } else {
            $blockheight = BLOCK_TUTORS_SETTINGS_DEFAULT_HEIGHT;
        }

        $tutorsjson = block_tutors_get_config_data($tutorrows, $allcourses['courses'], $this->uniqid, $currentpage, $alloweditprofile, $maxcolsperrow);
        $tutorsjson['alloweditprofile'] = $alloweditprofile;

        $tutorsjson['blockheight'] = $blockheight;
        $tutorsjson['minwidthcard'] = $minwidthcard;
        $tutorsjson['maxcolsperrow'] = $maxcolsperrow;

        $this->content->text = $OUTPUT->render_from_template('block_tutors/tutors', $tutorsjson);

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Allows multiple instances of the block.
     */
    public function instance_allow_multiple() {
        return false;
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        $config = get_config("block_tutors");

        // If title in settings is empty, hide header.
        if (!empty($this->config->title)) {
            return false;
        } else {
            return true;
        }
    }

}
