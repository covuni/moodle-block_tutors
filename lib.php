<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tutors block various functions and callbacks.
 *
 * @package block_tutors
 * @copyright 2019 Manoj Solanki (Coventry University)
 *
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
defined('MOODLE_INTERNAL') || die();

define( 'TUTORS_CACHE_TTL_DEFAULT', 300);

use block_tutors\myprofile\editprofile;

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/user/editlib.php');
require_once($CFG->dirroot . '/lib/setuplib.php');

// Cache names.
define( 'BLOCK_TUTORS_CACHE_TUTORS_DATA', 'blocktutorscoursetutorscachedata');
define( 'BLOCK_TUTORS_CACHE_TUTORS_KEY', 'blocktutorscoursetutorscachekey');
define( 'BLOCK_TUTORS_CACHE_MY_COURSES_DATA', 'blocktutorsmycoursescachedata');
define( 'BLOCK_TUTORS_CACHE_MY_COURSES_KEY', 'blocktutorsmycoursescachekey');

// These contain the actual bootstrap classes as defined in Boostrap 4.
define('BLOCK_TUTORS_SETTING_NONE', 'none');
define( 'BLOCK_TUTORS_BS_COLOR_LIGHT', 'light');
define( 'BLOCK_TUTORS_BS_COLOR_PRIMARY', 'primary');
define( 'BLOCK_TUTORS_BS_COLOR_SECONDARY', 'secondary');
define( 'BLOCK_TUTORS_BS_COLOR_SUCCESS', 'success');
define( 'BLOCK_TUTORS_BS_COLOR_DANGER', 'danger');
define( 'BLOCK_TUTORS_BS_COLOR_WARNING', 'warning');
define( 'BLOCK_TUTORS_BS_COLOR_INFO', 'info');
define( 'BLOCK_TUTORS_BS_COLOR_DARK', 'dark');

define ('BLOCK_TUTORS_BS_ALIGN_LEFT', 'text-left');
define ('BLOCK_TUTORS_BS_ALIGN_CENTER', 'text-center');
define ('BLOCK_TUTORS_BS_ALIGN_RIGHT', 'text-right');

define ('BLOCK_TUTORS_SORT_SURNAME', 0);
define ('BLOCK_TUTORS_SORT_LAST_ACCESSED', 1);
define ('BLOCK_TUTORS_SORT_LAST_UPDATED', 2);

define ('BLOCK_TUTORS_DEFAULT_ROLE_TO_SHOW', 3);    // Hardcoded 3 as this is the standard teacher role and has an id of 3.

// For defining a max length for display of a setting value.
// Used where dropdown select settings may contain a long descriptive text in the list of options.
define ('BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH', 50);
define ('BLOCK_TUTORS_SETTINGS_DEFAULT_HEIGHT', 380);
define ('BLOCK_TUTORS_SETTINGS_MIN_WIDTH_CARD', 230);
define ('BLOCK_TUTORS_SETTINGS_DEFAULT_MAX_COLS_PER_ROW', 4);

require_once($CFG->dirroot . '/user/lib.php');

/**
 * Get the current page to allow us to check if the block is allowed to display.
 *
 * @return string The page name, which is either "frontpage", "dashboard" or "coursepage", or empty string.
 *
 */
function block_tutors_get_current_page() {
    global $COURSE, $PAGE;

    // This will store the kind of activity page type we find. E.g. It will get populated with 'section' or similar.
    $currentpage = '';

    // We expect $PAGE->url to exist.  It should!
    $url = $PAGE->url;

    if ($PAGE->pagetype == 'site-index') {
        $currentpage = 'frontpage';
    } else if ($PAGE->pagetype == 'my-index') {
        $currentpage = 'dashboard';
    }

    // Check if course home page.
    if (empty ($currentpage)) {
        if ($url !== null) {
            // Check if this is the course view page.
            if (strstr ($url->raw_out(), 'course/view.php')) {

                // Get raw querystring params from URL.
                $getparams = http_build_query($_GET);

                // Check url paramaters.  Count should be 1 if course home page.
                // Checking that section param doesn't exist as an extra if relevant.  Also checking raw querystring defined
                // above.  This is due to section 0 not actually recording 'section' as a param.
                $urlparams = $url->params();

                $config = get_config('block_tutors');

                // Allow the block to display on course sections too if the relevant setting is on.
                if (!empty ($config->displayinsections)) {
                    if ((count ($urlparams) == 1) || (array_key_exists('section', $urlparams))) {
                        $currentpage = 'coursepage';
                    }
                } else {
                    if ((count ($urlparams) == 1) && (!array_key_exists('section', $urlparams))) {
                        $currentpage = 'coursepage';
                    }
                }

            }
        }
    }

    return $currentpage;
}

/**
 *
 * Get the tutors for the course.
 *
 * @param object   $course       Course object
 * @param string   $orderby      Sort order
 * @param string   $userfields   Fields to get for the tutor
 * @param bool     $iscoursepage Is this a course page?
 * @param array    $rolestoshow  An array containing role ids to show
 *
 * @return array The list of tutors
 *
 */
function block_tutors_get_tutors_for_course($course, $orderby, $userfields, $iscoursepage, $rolestoshow = array()) {

    global $OUTPUT, $USER;

    $config = get_config('block_tutors');

    $tempcoursetutorsdata = array();
    $coursecontext = context_course::instance($course->id);

    // Find the roles available on this course.
    $roles = array_reverse(get_default_enrol_roles($coursecontext, null), true);

    foreach ($roles as $key => $role) {
        $att = 'role_'.$key;

        // Check if we can display this role.
        if (! isset ($rolestoshow[$key]) ) {
            continue;
        }

        $contacts = get_role_users($key, $coursecontext, false, $userfields, $orderby, null, '', '', 30);

        foreach ($contacts as $contact) {

            // Used as a flag to store if this is the current logged in user.
            $currentuserisloggedinuser = false;

            // Used to store if user is allowed to edit their own profile.
            $alloweditprofile = false;

            // Check if this is the same user as the current user.
            if ($contact->id == $USER->id) {
                $currentuserisloggedinuser = true;

                // Check if edit profile is allowed to be edited. We check this here as we've
                // already identified that the user is in one of the defined roles to display tutors for.
                // E.g. Edit profile is allowed if user is in one of the tutor roles to display.
                if (!empty($config->alloweditprofile)) {
                    $alloweditprofile = true;
                }

            }

            // Get the user record for the current user only (although the method called supports multiple users).
            $user = user_get_users_by_id(array($contact->id));

            $messagetutor = '';
            if (!empty($config->showmessagetutor) ) {
                $messagetutor = block_tutors_message_user($user[$contact->id], $coursecontext);;
            }

            $name = $contact->firstname . ' ' . $contact->lastname;
            if (!empty($config->showalternatename)) {
                if (!empty($contact->alternatename)) {
                    $name = $contact->alternatename;
                }
            }

            if ($currentuserisloggedinuser) {
                $name .= ' (' . get_string('itsyou', 'block_tutors') . ')';
            }

            $phone = '';
            if (!empty($config->showphone)) {
                if (!empty($contact->phone1)) {
                    $phone = get_string('telephonenumbertitle', 'block_tutors') . $contact->phone1;
                }
                if (!empty($contact->phone2)) {
                    $phone .= ' | ' . $contact->phone2;
                }
            }

            // Description.

            // This will handle cases of using a read more link to view the full description,
            // which is done in cases where the length is more than the setting value 'maxlengthdescription'.
            $description = $descriptionexcerpt = $descriptionfull  = '';
            if (!empty($config->showdescription)) {
                if (!empty($contact->description)) {
                    if (!empty($config->maxlengthdescription)) {
                        $maxlen = $config->maxlengthdescription;
                    } else {
                        $maxlen = BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH;
                    }
                    if (strlen($contact->description) > $maxlen) {
                        $descriptionexcerpt = mb_strimwidth($contact->description, 0, $maxlen, ' ...', 'utf-8');
                        $descriptionfull = $contact->description;
                        $description = '';
                    } else {
                        $description = $contact->description;
                    }

                }
            }

            // Profile fields. Check and display any profile fields.
            $userprofilefields = profile_user_record($contact->id, false);

            // Job role / title.
            $jobtitlerole  = '';
            if ( (!empty($config->profilefieldfortitle)) &&
                ($config->profilefieldfortitle != 'none') ) {

                $profilefieldfortitle = $config->profilefieldfortitle;

                // First check if there is a value in the relevant profile field.
                if (!empty($userprofilefields->$profilefieldfortitle)) {
                    $jobtitlerole = $userprofilefields->$profilefieldfortitle;
                }
            }

            // Location / Office.
            $location = '';
            if ( (!empty($config->profilefieldforlocation)) &&
                ($config->profilefieldforlocation != 'none') ) {

                $locationprofilefield = $config->profilefieldforlocation;

                // First check if there is a value in the relevant profile field.
                if (!empty($userprofilefields->$locationprofilefield)) {

                    $location = $userprofilefields->$locationprofilefield;

                }
            }

            // Office hours.
            $officehours = $officehoursexcerpt = $officehoursfull  = '';
            if ( (!empty($config->profilefieldforofficehours)) &&
                ($config->profilefieldforofficehours != 'none') ) {

                $officehoursprofilefield = $config->profilefieldforofficehours;

                // First check if there is a value in the relevant profile field.
                if (!empty($userprofilefields->$officehoursprofilefield)) {

                    // Now separate into an excerpt if appropriate by using
                    // further variables to store a full and excerpt version.
                    $officehours = $userprofilefields->$officehoursprofilefield;

                    if (!empty($config->maxlengthofficehours)) {
                        $maxlen = $config->maxlengthofficehours;
                    } else {
                        $maxlen = BLOCK_TUTORS_SETTINGS_NAME_MAX_LENGTH;
                    }
                    if (strlen($officehours) > $maxlen) {
                        $officehoursexcerpt = mb_strimwidth($officehours, 0, $maxlen, ' ...', 'utf-8');
                        $officehoursfull = $officehours;
                        $officehours = '';
                    }

                }
            }

            // There should always be a user record.
            if (!empty ($user[$contact->id])) {
                $tempcoursetutorsdata[$contact->id] = array (
                    'id' => $contact->id,
                    'name' => $name,
                    'lastname' => $contact->lastname,
                    'firstname' => $contact->firstname,
                    'timemodified' => $contact->timemodified,
                    'lastaccess' => $contact->lastaccess,
                    'jobtitlerole' => $jobtitlerole,
                    'description' => $description,
                    'descriptionexcerpt' => $descriptionexcerpt,
                    'descriptionfull' => $descriptionfull,
                    'location' => $location,
                    'officehours' => $officehours,
                    'officehoursexcerpt' => $officehoursexcerpt,
                    'officehoursfull' => $officehoursfull,
                    'email' => !empty($config->showemail) ? $contact->email : '',
                    'picture' => $OUTPUT->user_picture($contact, array('size' => 150)),
                    'coursename' => $iscoursepage == false ? $course->shortname : '',
                    'messagetutor' => $messagetutor,
                    'phone' => !empty($config->showphone) ? $phone : '',
                    'alloweditprofile' => $alloweditprofile
                );
            }

        }

    }

    return $tempcoursetutorsdata;;

}

/**
 * This is to process a submitted user profile form from a Moodle fragment js call.
 *
 * @param array $args The fragment arguments.
 * @return mixed A custom message
 */
function block_tutors_output_fragment_save_profile($args) {
    global $CFG, $DB, $PAGE;

    $querystring = preg_replace('/^\?/', '', $args['querystring']);
    $params = [];
    parse_str($querystring, $params);
    $userid = $args['userid'];

    $editprofile = new editprofile();
    $formdata = editprofile::generate_tutor_edit_form($userid, $params);
    $mform = $formdata['form'];

    // Form processing.
    if ($mform->is_cancelled()) {
        // Handle form cancel operation, if cancel button is present on form. This won't happen but left check in anyway.
        return 'Form cancelld';
    } else if ($usertosave = $mform->get_data()) {

        $customlink = html_writer::link($PAGE->url, 'click here');
        $custommessage = get_string('saveddatamessage', 'block_tutors', $customlink);

        // User data has validated so we can save user data.
        $usertosave->timemodified = time();

        if ($usertosave->id == -1) {
            return 'No valid user found!';
        } else {
            $usercontext = context_user::instance($userid);
            $usertosave = file_postupdate_standard_editor($usertosave, 'description', $formdata['editoroptions'],
                    $usercontext, 'user', 'profile', 0);

            user_update_user($usertosave, false, false);

        }

         // Update user picture.
        if (empty($USER->newadminuser)) {
            \core_user::update_picture($usertosave, $filemanageroptions);
        }

        // Save custom profile fields data.
        profile_save_data($usertosave);

        // Reload from db.
        $usernew = $DB->get_record('user', array('id' => $usertosave->id));

        // Trigger update/create event, after all fields are stored. We assume this is always an update.
        \core\event\user_updated::create_from_userid($usertosave->id)->trigger();

        // Clear this user from cache.
        $cache = cache::make('block_tutors', BLOCK_TUTORS_CACHE_MY_COURSES_DATA);
        $result = $cache->delete($usertosave->id);

        return $custommessage;

    } else {
        // This branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
        // or on the first display of the form.

        // Set default data (if any).
        $mform->set_data();

        // We have to put this "Error" here as this is tied to the javascript to detect it incase we are not saving the data.
        $error = html_writer::start_tag('h5', array ('class' => 'text-center'));
        $error .= 'Error! ' . get_string ('datacouldnotbesavedmessage', 'block_tutors');
        $error .= html_writer::end_tag('h5');
        return $mform->render() . $error;
    }

}

/**
 *
 * Get the tutors for the course.
 *
 * @param array $courses      This contains an array of courses
 * @param int   $sortorder    The sort order
 * @param bool  $iscoursepage Is this a course page?
 *
 * @return array The list of tutors
 *
 */
function block_tutors_get_tutors_for_all_courses($courses, $sortorder = BLOCK_TUTORS_SORT_SURNAME, $iscoursepage = false) {

    global $OUTPUT;

    $tutors = array();
    $rolestoshow = array();

    // Get the roles we can display.
    $config = get_config('block_tutors');

    if (isset ($config->rolestoshow)) {
        $temprolestoshow = explode (',', $config->rolestoshow);

        foreach ($temprolestoshow as $key => $value) {
            $rolestoshow[$value] = true;
        }
    }

    if (empty($rolestoshow)) {
        return $tutors;
    }

    $userfields = 'u.id,u.lastaccess,u.firstname,u.lastname,u.email,u.phone1,u.phone2,u.picture,u.imagealt,
        u.firstnamephonetic,u.lastnamephonetic,u.middlename,u.alternatename,u.description, u.timemodified';

    // Contacts sort order.
    $orderby = 'u.lastname'; // Default.
    switch($sortorder) {
        case BLOCK_TUTORS_SORT_SURNAME:
            $orderby = 'u.lastname';
            break;
        case BLOCK_TUTORS_SORT_LAST_ACCESSED:
            $orderby = 'u.lastaccess DESC';
            break;
        case BLOCK_TUTORS_SORT_LAST_UPDATED:
            $orderby = 'u.timemodified';
            break;
        default:
            $orderby = 'u.lastname, u.firstname';
            break;
    }

    // Check if caching is being used.  If so, get all course contacts from cache if possible. Or add it to cache.
    if (!empty ($config->usecaching)) {

        $allcoursetutors = array();

        foreach ($courses as $course) {

            $cache = cache::make('block_tutors', BLOCK_TUTORS_CACHE_TUTORS_DATA);
            $coursetutorsdata = $cache->get($course->id);

            if ($coursetutorsdata == false) {

                $tempcoursetutorsdata = block_tutors_get_tutors_for_course($course, $orderby, $userfields,
                                        $iscoursepage, $rolestoshow);
                $coursetutorsforcache = array();
                $coursetutorsforcache['lastcachebuildtime'] = time();
                $coursetutorsforcache['tutors'] = $tempcoursetutorsdata;
                $allcoursetutors[$course->id] = $tempcoursetutorsdata;
                $cache->set($course->id, $coursetutorsforcache);

            } else {
                $allcoursetutors[$course->id] = $coursetutorsdata['tutors'];

                // Check if ttl expired for this user's courses.
                $timenow = time();
                if (isset ($config->cachingttl) ) {
                    $usercachettl = $config->cachingttl;
                } else {
                    $usercachettl = TUTORS_CACHE_TTL_DEFAULT;
                }

                // This should always be set.
                if (isset ($coursetutorsdata['lastcachebuildtime'])) {
                    if ($timenow > ($coursetutorsdata['lastcachebuildtime'] + $usercachettl) ) {

                        // Clear this user from cache.
                        $cache->delete($course->id);
                    }
                }
            }

        }

    } else {
        $allcoursetutors = array();

        foreach ($courses as $course) {

            $tempcoursetutorsdata = block_tutors_get_tutors_for_course($course, $orderby, $userfields,
                                    $iscoursepage, $rolestoshow);

            $allcoursetutors[$course->id] = $tempcoursetutorsdata;

        }
    }

    // Go through all course tutors found. This will be sanitised for duplicates etc after going through all courses.
    foreach ($allcoursetutors as $coursetutors) {

        foreach ($coursetutors as $coursetutor) {
            if (isset ($tutors[$coursetutor['id']])) {
                $tutors[$coursetutor['id']]['coursename'] .= ', ' . $coursetutor['coursename'];
            } else {
                $tutors[$coursetutor['id']] = array (
                    'id' => $coursetutor['id'],
                    'name' => $coursetutor['name'],
                    'lastname' => ucfirst($coursetutor['lastname']),
                    'firstname' => $coursetutor['firstname'],
                    'jobtitlerole' => $coursetutor['jobtitlerole'],
                    'email' => isset($config->showemail) ? $coursetutor['email'] : '',
                    'picture' => $coursetutor['picture'],
                    'messagetutor' => $coursetutor['messagetutor'],
                    'phone' => $coursetutor['phone'],
                    'location' => $coursetutor['location'],
                    'officehours' => clean_param($coursetutor['officehours'], PARAM_CLEANHTML),
                    'officehoursexcerpt' => clean_param($coursetutor['officehoursexcerpt'], PARAM_CLEANHTML),
                    'officehoursfull' => clean_param($coursetutor['officehoursfull'], PARAM_CLEANHTML),
                    'description' => clean_param($coursetutor['description'], PARAM_CLEANHTML),
                    'descriptionexcerpt' => clean_param($coursetutor['descriptionexcerpt'], PARAM_CLEANHTML),
                    'descriptionfull' => clean_param($coursetutor['descriptionfull'], PARAM_CLEANHTML),
                    'coursename' => $iscoursepage == false ? $coursetutor['coursename'] : '',
                    'alloweditprofile' => $coursetutor['alloweditprofile'],
                    'timemodified' => $coursetutor['timemodified'],
                    'lastaccess' => $coursetutor['lastaccess']
                );
                if ( !empty($coursetutor['editprofile'])) {
                    $tutors[$coursetutor['id']]['editprofile'] = $coursetutor['editprofile'];
                }
            }
        }
    }

    switch($sortorder) {
        case BLOCK_TUTORS_SORT_SURNAME:
            usort($tutors, function ($a, $b) {
                return $a['lastname'] < $b['lastname'];
            });
            break;
        case BLOCK_TUTORS_SORT_LAST_ACCESSED:
            usort($tutors, function ($a, $b) {
                return $a['lastaccess'] < $b['lastaccess'];
            });
            break;
        case BLOCK_TUTORS_SORT_LAST_UPDATED:
            usort($tutors, function ($a, $b) {
                return $a['timemodified'] < $b['timemodified'];
            });
            break;
        default:
            usort($tutors, function ($a, $b) {
                return $a['lastname'] < $b['lastname'];
            });
            break;
    }

    return $tutors;

}

/**
 *
 * Get data, mainly config information for the block and put into an array to be used as JSON data when rendering a template.
 *
 * @param array  $tutorrows        List of tutors organised into an array mapped to rows
 * @param array  $allcourses       List of user's courses
 * @param int    $uniqid           A unique id for identifying the block
 * @param string $currentpage      Hold whether this is a course or dashboard / frontpage.
 * @param bool   $alloweditprofile Flag that determines whether to generate the Moodle edit profile form.
 * @param int    $maxcolsperrow    Maximum number of tutors to display in a row
 *
 * @return string html from template
 *
 */
function block_tutors_get_config_data($tutorrows, $allcourses, $uniqid, $currentpage, $alloweditprofile = true,
                                      $maxcolsperrow = BLOCK_TUTORS_SETTINGS_DEFAULT_MAX_COLS_PER_ROW) {

    global $OUTPUT, $USER;

    $config = get_config("block_tutors");

    // If any title text is configured.
    $title = '';
    if (!empty($config->tutorstitleext)) {
        $title = $config->tutorstitletext;
    }

    // Get styling options.
    if ( (!empty($config->bsbackgroundcolor)) && ($config->bsbackgroundcolor != 'none')) {
        $bsbackgroundcolor = 'bg-' . $config->bsbackgroundcolor;
    } else {
        $bsbackgroundcolor = '';
    }

    if ( (!empty($config->bstextcolor)) && ($config->bstextcolor != 'none') ) {
        $bstextcolor = 'text-' . $config->bstextcolor;
    } else {
        $bstextcolor = '';
    }

    if ( (!empty($config->bstextalign)) ) {
        $bstextalign = $config->bstextalign;
    } else {
        $bstextalign = BLOCK_TUTORS_BS_ALIGN_LEFT;
    }

    // Other config settings.
    if (!empty($config->showdescription) ) {
        $showdescription = true;
    } else {
        $showdescription = false;
    }

    if (!empty($config->showemail) ) {
        $showemail = true;
    } else {
        $showemail = false;
    }

    if (!empty($config->showmessagetutor) ) {
        $showmessagetutor = true;
    } else {
        $showmessagetutor = false;
    }

    if (!empty($config->showphone) ) {
        $showphone = true;
    } else {
        $showphone = false;
    }

    if (!empty($config->showalternatename) ) {
        $showalternatename = true;
    } else {
        $showalternatename = false;
    }

    // Get language strings for use in template.
    $readmoretext = get_string('readmoretext', 'block_tutors');
    $showlesstext = get_string('showlesstext', 'block_tutors');
    $descriptiontitle = get_string('descriptiontitle', 'block_tutors');
    $officehourstitle = get_string('officehourstitle', 'block_tutors');
    $locationtitle = get_string('locationtitle', 'block_tutors');

    $extraclasses = ' ' . $bsbackgroundcolor . ' ' . $bstextcolor . ' ' . $bstextalign . ' ';

    $usercourses = array();

    foreach ($allcourses as $course) {
        $course->shortname = mb_strimwidth($course->shortname, 0, 20, '...', 'utf-8');
        $usercourses[] = (array) $course;
    }

    if ($alloweditprofile) {
        $editprofile = new editprofile();
        $form = $editprofile::generate_tutor_edit_form($USER->id);

        if ($form != null) {
            $editform = $form['form']->render();
        } else {
            $editform = html_writer::start_tag('h3', array ('class' => 'text-center text-danger'));
            $editform .= get_string ('cannotdisplayeditprofileform', 'block_tutors');
            if (!has_capability('moodle/user:editownprofile', context_user::instance($USER->id))) {
                $editform .= '<br><br>' . get_string ('cannoteditownprofile', 'block_tutors');
            }
            $editform .= html_writer::end_tag('h3');
        }
    } else {
        $editform = '';
    }

    // Setting for showing the courses the tutor is enrolled on.
    $showtaughtcourses = false;
    if ($currentpage != 'coursepage') {
        if (!empty($config->showtaughtcourses)) {
            $showtaughtcourses = true;
        }

    }

    $notutorsfoundtext = '';

    if (empty ($tutorrows)) {
        $notutorsfoundtext = get_string ('nocontentdisplaymessagetext', 'block_tutors');
    }

    $tutorsjson = new stdClass();
    $tutorsjson = array ("uniqid" => $uniqid, "id" => $USER->id, "editform" => $editform,
            "tutors" => $tutorrows,
            "extraclasses" => $extraclasses, "grid-size" => 12 / $maxcolsperrow,
            "title" => $title, "showdescription" => $showdescription,
            "showemail" => $showemail, "showmessagetutor" => $showmessagetutor,
            "showphone" => $showphone, "showalternatename" => $showalternatename,
            "readmoretext" => $readmoretext, "showlesstext" => $showlesstext,
            "descriptiontitle" => $descriptiontitle, "locationtitle" => $locationtitle,
            "officehourstitle" => $officehourstitle, "allcourses" => $usercourses,
            "showtaughtcourses" => $showtaughtcourses,
            'notutorsfoundtext' => $notutorsfoundtext
    );

    return $tutorsjson;

}

/**
 * Return output to message the user.
 *
 * @param obj $user    The user object
 * @param obj $context The context to use in capability check
 *
 * @return string Output of message user
 */
function block_tutors_message_user($user, $context) {
    global $USER, $OUTPUT, $CFG;

    $messageuser = '';
    // Check to see if we should be displaying a message button.
    if (!empty($CFG->messaging) && $user->id != $USER->id && has_capability('moodle/site:sendmessage', $context)) {

        $anchortagcontents = $OUTPUT->pix_icon('t/message', get_string('messageselectadd'));
        $anchorurl = new moodle_url('/message/index.php', array('id' => $user->id));
        $anchortag = html_writer::link($anchorurl, $anchortagcontents,
                array('title' => get_string('message', 'message')));

        $messageuser = $anchortag;
    }

    return $messageuser;
}
